students = {
    "Eren Eger" : { "Group" : 110, "Course" : 1, "Grades" : { "OOP" : 5, "NM" : 4, "Python" : 5, "Economy" : 4 } },
    "Mikasa Akerman" : { "Group" : 111, "Course" : 1, "Grades" : { "OOP" : 4, "NM" : 4, "Python" : 3, "Economy" : 5 } },
    "Jean Kirschtein" : { "Group" : 111, "Course" : 1, "Grades" : { "OOP" : 3, "NM" : 3, "Python" : 4, "Economy" : 3 } },
    "Conny Springer" : { "Group" : 110, "Course" : 1, "Grades" : { "OOP" : 5, "NM" : 3, "Python" : 4, "Economy" : 3 } },
    "levi Ackermann" : { "Group" : 211, "Course" : 2, "Grades" : { "OOP" : 3, "NM" : 4, "Python" : 5, "Economy" : 4 } },
    "Hange Zoe" : { "Group" : 210, "Course" : 2, "Grades" : { "OOP" : 5, "NM" : 5, "Python" : 4, "Economy" : 4 } },
    "Sasha Braus" : { "Group" : 211, "Course" : 2, "Grades" : { "OOP" : 4, "NM" : 5, "Python" : 5, "Economy" : 3 } },
    "Erwin Smith" : { "Group" : 311, "Course" : 3, "Grades" : { "OOP" : 4, "NM" : 4, "Python" : 5, "Economy" : 5 } },
    "Marlo Freudenberg" : { "Group" : 311, "Course" : 3, "Grades" : { "OOP" : 4, "NM" : 3, "Python" : 4, "Economy" : 3 } },
    "Miche Zacharius" : { "Group" : 310, "Course" : 3, "Grades" : { "OOP" : 3, "NM" : 5, "Python" : 3, "Economy" : 3 } },
}
def main():
    print_students()
    add_student("Oruo Bozard", 210, 2, 4, 3, 5, 4 )
    add_student("Oruo Bozard", 210, 2, 4, 3, 5, 4 )
    print_students()


#? Скрипченко Д. Створив словник студентів та дві функції для виводу всіх студентів в консоль на додавання нового студента до словника 
def add_student(name : str, group : int, course : int, oop : int, nm : int, python: int, economy: int):
    if not name in students:
        students[name] = {"Group" : group, "Course" :course, "Grades" : {"OOP" : oop, "NM" : nm, "Python" : python, "Economy" : economy} }
        print(f"•••••Student {name} successfully added to students list!•••••")
    else:
        print(f"•••••Student {name} already exists•••••")

def delete_student(name : str):
    if name in students:
        del students[name]
        print(f"•••••Student {name} has successfully been deleted from dictionary!•••••")
    else: 
        print(f"•••••Student {name} with such name doesn't exist in this dictionary!\nTry different name please!•••••")

def print_students():
    for student in students:
        print(f"student {student}, group: {students[student]['Group']}, course: {students[student]['Course']}")
        for grade in students[student]["Grades"]:
            print(f"{grade} : {students[student]['Grades'][grade]}", end="\t")
        print(end="\n")
        
# TODO наступний студент має додати функцію сортування студентів у словнику за іменами


if __name__ == "__main__":
    main()